# Restorm

Restorm is an ExpressJS supporting library that lets you easily create RESTful JSON APIs in a true MVC fashion

# Installation

```bash
npm install restorm --save
```


# Usage

## In your express app.js

```js
var express = require('express');
var Restorm = require('restorm');

var app = express();

app.use(Restorm.init({

  //pass in your app instance
  app:app,
  
  //required to resolve paths to your models, transforms and controllers
  appRoot:path.resolve(__dirname),
  
  //express object require to create routers
  express:express,
  
  //datasource to use. Will be cool to switch datasources to use a common api but merp!
  datasource:'restorm-memory',
  
  //REST URL base path
  mountApi:'/api'
}));
```
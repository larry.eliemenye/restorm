/**
 * Created by tthlex on 16/01/16.
 */

var fs = require('fs');
var path = require('path');
var _ = require('lodash');

function loadFilesSync(filePath){
    var files = fs.readdirSync(filePath);
    var data = [];
    files.forEach(function(file){
        data.push(require(path.resolve(filePath, file)));
    });
    return data;
}

//create routes for all the controllers
function RouteCreator(options, controllers){
    var BASEPath = '/';
    var routeInterface = {};

    //conventional routehandler function names used in the controllers
    var routeMethods = {
        'get':'handleGet',
        'post':'handlePost',
        'delete':'handleDelete',
        'put':'handlePut'
    };

    //create parent
    var parentRouter = options.express.Router();

    //get all defined controllers and associate them with respective routers
    controllers.forEach(function(controller){

        //get controller internals
        var ctrlr = controller.call(null);

        //create a route for this controller
        var ctrlrRouter = options.express.Router();

        //assigns routes
        for(var method in routeMethods){
            var handlerName = routeMethods[method];
            ctrlrRouter[method].call(
                ctrlrRouter,
                ctrlr.MOUNT_PATH,
                ctrlr[handlerName]
            );
        }

        //attach to parent route
        parentRouter.use(BASEPath, ctrlrRouter);
    });

    routeInterface.parentRouter = parentRouter;
    return routeInterface;
}
function setupControllers(options, controllers, models, transforms){
    //initialize express router
    var RestormRouter = options.express.Router();
    RestormRouter.use(options.mountApi, RouteCreator(options, controllers).parentRouter);
    return RestormRouter;
};

/**
 *
 * - you need to pass in appRoot directory to tell restorm how to find your models, controller and transformations files
 *
 * */
module.exports = function(options){

    var defaultOptions = {};
    var appRoot = options.appRoot;

    //resolve default dirs
    defaultOptions.modelDir = path.resolve(appRoot, 'models');
    defaultOptions.transformDir = path.resolve(appRoot, 'transforms');
    defaultOptions.controllerDir = path.resolve(appRoot, 'controllers');

    //api path
    defaultOptions.mountApi = '/';

    //resolve options
    var useOptions = _.merge(defaultOptions, options);

    //load files
    var controllers = loadFilesSync(useOptions.controllerDir);
    var models = loadFilesSync(useOptions.modelDir);
    var transforms = loadFilesSync(useOptions.transformDir);

    //load controllers, models, and transformations
    return setupControllers.call(null, useOptions, controllers, models, transforms);
};
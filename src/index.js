/**
 * Created by tthlex on 16/01/16.
 */
var loader = require('./loader');
var model = require('./BaseModel');
var Schema = require('jugglingdb').Schema;
var RestormSchema;
module.exports.init = function(options){
    return loader(options);
};
module.exports.Model = model;
module.exports.Schema = Schema;
//module.exports.Controller = controller;